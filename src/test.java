/**
 * @author Данил Востроилов
 * @version 2.2.8
 * @since 22
 * Тестовый класс
 */
public class test {
    /**
     * Переменная имени
     */
    private String name;

    /**
     * Конструктор класса test
     * @param name -- переменная имени
     */
    public test(String name) {
        this.name = name;
    }

    /**
     * Пустой конструктор
     */
    public test() {

    }

    /**
     * Геттер имени
     * @return возращает имя
     */
    public String getName() {
        return name;
    }

    /**
     * Функция которая печатает имя и товар
     * @param good
     */
    public void buy(String good){
        System.out.println(name + " купил товар: " + good);
    }


}
